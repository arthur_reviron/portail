<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="login.css">
</head>
<body>
<svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-info-circle info" fill="currentColor"
     xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
    <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588z"/>
    <circle cx="8" cy="4.5" r="1"/>
</svg>
<div class="card" style="width: 22rem; margin:15% auto">
    <form action="../curl/curl.php" method="get">
        <div class="form-group">
            <label for="exampleInputEmail1">Connexion magasin</label>
            <input type="email" class="form-control" name="shopEmail" id="shopEmail" aria-describedby="emailHelp"
                   placeholder="email magasin">
            <small id="emailHelp" class="form-text text-muted">Entrez le mail de votre magasin</small>
        </div>
        <button type="submit" class="btn btn-primary">Connexion</button>
    </form>
</div>
</body>
</html>
