<?php if (!session_id()) session_start(); ?>
<div class="header">
    <div class="header--shop">
        <?php

        use Firebase\JWT\JWT;

        if ($_SESSION['token'] && $_COOKIE['token']) {
            $jwt = $_COOKIE['token'];
        }
        if (!$_SESSION['token']) {
            $jwt = $_COOKIE['token'];
        }
        if (!$_COOKIE['token']) {
            $jwt = $_SESSION['token'];
            $_COOKIE['token'] = $_SESSION['token'];
        }
        $shopInfo = explode('}', JWT::urlsafeB64Decode($jwt), 3)[1];
        $shopInfo .= '}';
        $shopInfo = json_decode($shopInfo);
        $getShopInfoResonse = ShopName::getShopName($shopInfo->shopId);
        $magasin = $getShopInfoResonse === 1 ?
            'Magasin non reconnu: ' . $shopInfo->shopEmail :
            json_decode($getShopInfoResonse, true)['point_of_sale']['name'];
        $_SESSION['noMagasin'] = $shopInfo->shopId;
        echo '<h1>' . $magasin . '</h1>';
        ?>
    </div>
    <div class="header--shop-number">
        <?php
        echo '<h2>#' . $_SESSION['noMagasin'] . '</h2>';
        ?>
    </div>
</div>


