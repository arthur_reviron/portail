<?php
require 'const.php';

$dsn = 'pgsql:host=' . Db::HOST . ';port=' . Db::PORT . ';dbname=' . Db::DB_NAME . ';user=' . Db::USERNAME . ';password=' . Db::PWD;

try {
    $conn = new PDO($dsn);
} catch (PDOException $e) {
    echo $e->getMessage();
}


