<?php if (!session_id()) session_start();
if ($_GET['token']) {
    setcookie('token', $_GET['token'], time() + (10 * 365 * 24 * 60 * 60));
    $_SESSION['token'] = $_GET['token'];
}
if (!$_SESSION['token'] && !$_COOKIE['token']) {
    header("Location: ./../login/login.php");
}
require './curl/getShopName.php';
require './vendor/autoload.php';
require './db/db.php';
require './vendor/firebase/php-jwt/src/JWT.php';
require './entities/categories.php';
require './entities/tutorials.php';
require './entities/titles.php';
require './templates/homePage/homepage.php';
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Portail</title>
    <link rel="stylesheet" href="https://use.typekit.net/fnr0ylt.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="./templates/header/header.css">
    <link rel="stylesheet" href="index.css">
</head>
<body>
<?php
include './templates/header/header.php';

function displayWebsiteBloc($row, $category)
{
    if ($row['category'] === $category) {
        $url = $row['url'];
        if ($row['token']) {
            $url .= $_SESSION['token']; // append shop token on urls that need it
        }

        $tutoContent = getTuto($row, $category);

        echo '
        <div class="box--all ' . $row['css_class'] . '">
            <div class="center">
            <a  target="_blank"
                href=' . $url . '>
                <div class="box--icon">
                    <img src="sources/' . $row['icon'] . '"/>
                </div>
                <div class="box--title ' . $row['css_class'] . '">
                    ' . mb_strtoupper($row['title']) . '
                </div>
            </a>
            ' . $tutoContent .
            '</div>
            </div>';
    }
}

function getTuto($row, $category)
{
    if ($row['url_tuto']) {
        return '<a class="box--tuto badge rounded-pill ' . Tutorials::TUTORIALS[$category] . '" 
            target="_blank" 
            href="' . $row['url_tuto'] . '">tutoriel
         </a>
        ';
    }
}


$conn->exec('SET search_path TO sh_tools');
echo '<div class="page">';
foreach (Categories::CATEGORIES as $category) {
    echo '<div class="page--category">';
    $i = 0;
    $prep = $conn->query('SELECT * FROM t_app_list 
                                INNER JOIN t_app_shop ON t_app_list.app_id = t_app_shop.app_id
                                WHERE t_app_shop.shop_id = ' . $_SESSION['noMagasin'] . 'ORDER BY t_app_list.name ASC');
    foreach ($prep as $row) {
        if ($row['category'] === $category) {
            $i++;
        }
    }
    $prep = $conn->query('SELECT * FROM t_app_list 
                                INNER JOIN t_app_shop ON t_app_list.app_id = t_app_shop.app_id
                                WHERE t_app_shop.shop_id = ' . $_SESSION['noMagasin'] . 'ORDER BY t_app_list.name ASC');
    if ($i > 0) {
        echo '<h3 class="text-white">' . $category . '<hr/ class="' . Titles::TITLES[$category] . '"></h3>';
    }

    echo '<div class="containerz">';
    foreach ($prep as $row) {
        displayWebsiteBloc($row, $category);
    }
    echo '</div>';
    echo '</div>';
    if ($i > 0) {
    }
}
?>
</body>
</html>
