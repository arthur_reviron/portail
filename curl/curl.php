<?php
session_start();
?>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="login.css">
</head>
<?php
require '../entities/url.php';

if (isset($_GET['shopEmail'])) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://us-central1-moncompte-mb-prod.cloudfunctions.net/requestMagicLink?callbackURL=' . Url::CALLBACK_URL . '&email=' . $_GET['shopEmail'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
    ));

    $response = curl_exec($curl);
//    echo $response;
    curl_close($curl);
    echo '<div class="alert alert-success" role="alert">
  Mail envoyé!
</div>';
}
?>
</html>
