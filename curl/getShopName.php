<?php

use GuzzleHttp\Exception\RequestException;

class ShopName
{
    static function getShopName($shopId)
    {
        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('GET', 'https://pointdevente.groupe-blachere.com/api/point-of-sales/' . $shopId);
            return $response->getBody();

        } catch (GuzzleHttp\Exception\RequestException $exception) {
            return 1;
        }
    }
}
