<?php

class Tutorials
{
    public const TUTORIALS = [
        'Intranet' => 'blue-custom-color--tuto',
        'Commande fournisseur' => 'purple-custom-color--tuto',
        'Mon commerce' => 'green-custom-color--tuto',
        'Mon compte' => 'orange-custom-color--tuto',
        'Marketing' => 'deepblue-custom-color--tuto',
        'Support' => 'pink-custom-color--tuto',
        'Mes produits' => 'lightgreen-custom-color--tuto',
    ];
}
