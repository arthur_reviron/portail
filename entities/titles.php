<?php

class Titles
{
    public const TITLES = [
        'Intranet' => 'blue-custom-color',
        'Commande fournisseur' => 'purple-custom-color',
        'Mon commerce' => 'green-custom-color',
        'Mon compte' => 'orange-custom-color',
        'Marketing' => 'deepblue-custom-color',
        'Support' => 'pink-custom-color',
        'Mes produits' => 'lightgreen-custom-color',
    ];
}
