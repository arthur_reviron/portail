<?php

class Url
{
    public const MAGIC_LINK_ENDPOINT = 'https://us-central1-moncompte-mb-prod.cloudfunctions.net/requestMagicLink';
    public const SHOP_INFOS_URL = 'https://pointdevente.groupe-blachere.com/api/point-of-sales/';
    public const CALLBACK_URL = 'http://localhost:8085/index.php';
}
