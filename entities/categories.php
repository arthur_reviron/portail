<?php

class Categories
{
    public const CATEGORIES = [
        'Intranet',
        'Commande fournisseur',
        'Mon commerce',
        'Mon compte',
        'Marketing',
        'Support',
        'Logiciel',
        'Mes produits',
    ];

}
